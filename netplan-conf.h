#ifndef _NETPLAN_CONF_H
#define _NETPLAN_CONF_H

#include "netcfg.h"

/* Constants for maximum size for Netplan config fields. */
#define NETPLAN_MAX_LEN_BUF     1024 /* Max len for most buffers */

/* Some Netplan default values. */
#define NETPLAN_CONFIG_FILE_PATH          "/etc/netplan"
#define NETPLAN_INSTALLER_FILE            "00-installer-config.yaml"

/* Public functions */
void netplan_write_configuration(const struct netcfg_interface *niface);

#endif
